from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'polls', views.QuestionViewSet)

urlpatterns = [
    path('', views.IndexView.as_view(), name="home"),
    path('<int:pk>', views.QuestionDetailView.as_view(), name='detail'),
    path('<int:question_id>/results', views.results, name='results'),
    path('<int:question_id>/vote', views.vote, name='vote'),
    path('api/v0/', views.api_root_view, name='api_root'),
    path('api/v0/polls/', views.QuestionAPIListView.as_view(), name='api_polls'),
    path('api/v0/polls/<int:pk>', views.QuestionAPIDetailView.as_view(), name='api_question_detail'),
    path('api/v0/choices/', views.ChoiceAPIListView.as_view(), name='api_choices'),
    path('api/v0/choices/<int:pk>', views.ChoiceAPIDetailView.as_view(), name='api_choices_detail'),
    path('api/v1/', include(router.urls)),
]
